# Installation

## Dependencies

```
# Git and php
sudo apt install git-core php -y
sudo apt install mariadb-server mariadb-client -y

# Composer
curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer

# Wiring pi
git clone git://git.drogon.net/wiringPi
cd wiringPi/
./build

# RPI Radio Frequency
apt-get install python3-pip
pip3 install rpi-rf

# pi Home Easy
git clone https://github.com/nbogojevic/piHomeEasy.git
cd piHomeEasy
make
sudo make install
```

## Deployment / Updates

```
./shell/deploy.sh
```

## Butler

```
mkdir -p /var/www/butler/butler/master/
cd /var/www/butler/butler/master/
git clone git@bitbucket.org:landa/butler.git ./
composer install
cp .env.example .env
vim .env
./artisan migrate
```

## Supervisor

**/etc/supervisor/conf.d/butler.conf**

```
[program:butler]
command=sudo ./artisan queue:work
directory=/var/www/butler/butler/master/
stdout_logfile=/var/www/butler/butler/master/storage/logs/supervisord.log
redirect_stderr=true
numprocs=5
process_name=%(program_name)s_%(process_num)02d
autostart=true
autorestart=true
```

## Cron 

```
sudo su
crontab -e
```

```
* * * * * cd /var/www/butler/butler/master && ./artisan schedule:run >> storage/logs/schedule.log 2>&1
```
