#!/bin/sh
RESULT=`git status -s`
if [ "$RESULT" ]; then
    echo "There are untracked changes"
else
    git reset --hard origin/master
    composer install
    ./artisan migrate
    ./artisan queue:restart
fi;
