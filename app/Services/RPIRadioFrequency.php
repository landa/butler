<?php

namespace App\Services;

use App\Contracts\RadioFrequency;
use App\RadioFrequencySignal;

class RPIRadioFrequency implements RadioFrequency
{
    public static $proc;
    public function receive()
    {
        $command = env('RF_RECEIVE_BIN', 'rpi-rf_receive');
        $exec = "$command 2>&1";
        if (! static::$proc) {
            static::$proc = popen($exec, 'r');
            register_shutdown_function(__CLASS__ . '::shutdown');
        }
        while (!feof(static::$proc))
        {
            $line = trim(fgets(static::$proc));
            // echo "R: $line\n";
            $rf = resolve('App\RadioFrequencySignal');
            $rf->fill([
                'code' => $line,
            ]);
            usleep(50000);
            return $rf;

        }
        pclose(static::$proc);
    }

    public static function shutdown() {
        if (! static::$proc) {
            return;
        }
        // Cleanup should happen automatically.
        // Just in case.
        pclose(static::$proc);
    }
}
