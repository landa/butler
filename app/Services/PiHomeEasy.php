<?php

namespace App\Services;

use App\Contracts\HomeEasy;
use App\HomeEasySignal;

class PiHomeEasy implements HomeEasy
{
    public function send(HomeEasySignal $signal)
    {
        $command = env('HOME_EASY_BIN', 'piHomeEasy');
        $pin     = env('HOME_EASY_PIN', '0');
        $onoff   = $signal->io ? 'on' : 'off';
        $exec = "$command $pin {$signal->emitter_id} {$signal->receiver_id} $onoff 2>&1";
        exec(
            $exec,
            $output,
            $result
        );
        if ($result) {
            throw new \Exception("Tried: $exec\n".implode("\n", $output), 1);
        }
    }
}
