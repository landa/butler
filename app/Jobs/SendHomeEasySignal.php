<?php

namespace App\Jobs;

use App\HomeEasySignal;
use App\Contracts\HomeEasy;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendHomeEasySignal implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $signal;

    public $tries = 5;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(HomeEasySignal $signal)
    {
        $this->signal = $signal;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(HomeEasy $homeEasy)
    {
        // Invoked by `SendHomeEasySignal::dispatch($signal)`;
        $homeEasy->send( $this->signal );
    }
}
