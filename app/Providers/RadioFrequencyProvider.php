<?php

namespace App\Providers;

use App\Contracts\RadioFrequency;
use App\Services\RPIRadioFrequency;
use Illuminate\Support\ServiceProvider;

class RadioFrequencyProvider extends ServiceProvider
{
    public $singletons = [
        RadioFrequency::class => RPIRadioFrequency::class,
    ];
}
