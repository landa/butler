<?php

namespace App\Providers;

use App\Contracts\HomeEasy;
use App\Services\PiHomeEasy;
use Illuminate\Support\ServiceProvider;

class HomeEasyProvider extends ServiceProvider
{
    public $singletons = [
        HomeEasy::class => PiHomeEasy::class,
    ];
}
