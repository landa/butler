<?php

namespace App\Events;

use App\Contracts\RadioFrequency;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;


class RadioFrequencyReceived
{
    use Dispatchable, SerializesModels;

    public $rf;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(RadioFrequency $rf)
    {
        $this->rf = $rf;
    }
}
