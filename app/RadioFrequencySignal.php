<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RadioFrequencySignal extends Model
{
    protected $table = 'radio_frequency_signals';
    public $fillable = [ 'code', 'pulselength', 'protocol' ];
}
