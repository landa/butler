<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomeEasySignal extends Model
{
    protected $table = 'home_easy_signals';
    public $fillable = [ 'emitter_id', 'receiver_id', 'io' ];
}
