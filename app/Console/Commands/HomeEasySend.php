<?php

namespace App\Console\Commands;

use App\HomeEasySignal;
use App\Jobs\SendHomeEasySignal;
use Illuminate\Console\Command;

class HomeEasySend extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'homeeasy:send {receiver_id} {emitter_id} {io}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $signal = new HomeEasySignal();
        $signal->fill([
            'emitter_id' => $this->argument('emitter_id'),
            'receiver_id' => $this->argument('receiver_id'),
            'io' => $this->argument('io')
        ]);
        $signal->save();
        SendHomeEasySignal::dispatch($signal);
    }
}
