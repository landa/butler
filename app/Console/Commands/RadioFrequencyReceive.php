<?php

namespace App\Console\Commands;

use App\RadioFrequencySignal;
use App\Contracts\RadioFrequency;
use App\Events\RadioFrequencyReceived;
use Illuminate\Console\Command;

class RadioFrequencyReceive extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rf:receive';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Continously receive RF signals';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(RadioFrequency $rf)
    {
        while($signal = $rf->receive()) {
            // Door/window maybe opened
            event(new RadioFrequencyReceived($rf));
        }
    }
}
