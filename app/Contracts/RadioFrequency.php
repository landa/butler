<?php

namespace App\Contracts;

use App\RadioFrequencySignal;

interface RadioFrequency
{
    public function receive();
}
