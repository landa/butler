<?php

namespace App\Contracts;

use App\HomeEasySignal;

interface HomeEasy
{
    public function send(HomeEasySignal $signal);
}
